var NoticeAdd = {
	data: {
		baseUrl: ''
    },
	init: function(baseUrl) {
		NoticeAdd.data.baseUrl = baseUrl;
		// 绑定click事件
		$('#addForm').on('click', '.addNotice', NoticeAdd.addNotice);
	},
	addNotice: function () {
		var title = $('#addForm input[name=title]').val();
        var content = $('#addForm textarea[name=content]').val();
        if (!(title && content)) {
            alert("表单内容有误");
            return;
        }
        var data = {};
        data.title = title;
        data.content = content;

        $.ajax({
            url: NoticeAdd.data.baseUrl + 'notice/insert',
            type: 'POST',
            dataType: 'json',
            contentType: 'application/json',
            data: JSON.stringify(data),
            success: function (response) {
                alert(response.message);
                if (response.type == 'success') {
            		window.location.href= NoticeAdd.data.baseUrl + 'notice/list';
				}
            }
        });
    }
	
}