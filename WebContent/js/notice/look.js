var NoticeLook = {
    data: {
        baseUrl: ''
    },
	init: function(baseUrl, jtstore) {
        NoticeLook.data.baseUrl = baseUrl;
        Main.showShop(jtstore);
        $('form .update').on('click', NoticeLook.updateCar);
	},
    updateCar: function () {
        var id = $('#noticeForm input[name=id]').val();
        var title = $('#noticeForm input[name=title]').val();
        var content = $('#noticeForm textarea[name=content]').val();

        if (!(id && title && content)) {
            alert("表单内容有误");
            return;
        }
        var data = {};
        data.id = id;
        data.title = title;
        data.content = content;

        $.ajax({
            url: NoticeLook.data.baseUrl + 'notice/update',
            type: 'POST',
            dataType: 'json',
            contentType: 'application/json',
            data: JSON.stringify(data),
            success: function (response) {
                if (response.type == 'success') {
                    window.location.href = NoticeLook.data.baseUrl + '/notice/list';
                } else {
                    alert(response.message);
                }
            }
        });
    }
}