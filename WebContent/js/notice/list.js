var laypage;
var NoticeList = {
	data: {
		baseUrl: '',
		lineSize: 8,
		totalPage: 1,
        totalData: 1
	},
	init: function(baseUrl, a) {
        Main.dateFormat();
        laypage = a;
		NoticeList.data.baseUrl = baseUrl;
		NoticeList.select();
		// 绑定click事件
		$('#conditionForm').on('click', '.select', NoticeList.select);
	},
    select: function () {
      NoticeList.page(1);
    },
	// 分页查询
	page: function (pageIndex) {
		// 获取查询条件
		var params = {};
        var title = $('#conditionForm input[name=title]').val();
        params.title = title;

        // 初始化data
		var data={
            pageIndex: pageIndex,
            lineSize: NoticeList.data.lineSize,
            params: params
		};
		// ajax post
		$.ajax({
			url: NoticeList.data.baseUrl + 'notice/page',
			type: 'POST',
            dataType: 'json',
            contentType: 'application/json',
			data: JSON.stringify(data),
            success: function (response) {
				if(response.type == 'success') {
                    console.log(response.message);
                    var page = response.data;
                    // 同步总页数
                    NoticeList.data.totalPage = page.totalPageCount;
                    // 总条数
					NoticeList.data.totalData = page.totalDataCount;
					var data = page.data;
					var noticeListHtml = '';
					$.each(data, function (index, item) {
					    var info = item.content;
                        info = info ? (info.length > 20 ? (info.substring(0, 20) + "...") : info) : '';
                        noticeListHtml += '<tr id="'+ item.id +'">' +
                            '<td>'+ item.id +'</td>' +
                            '<td>'+ item.title +'</td>' +
                            '<td style="width=400px;">'+ info +'</td>' +
                            '<td>'+ new Date(item.releasedate).format("yyyy-MM-dd HH:mm:ss") +'</td>' +
                            '<td><a href="'+ NoticeList.data.baseUrl +'notice/look?id='+ item.id +'">编辑&nbsp;</a>' +
                            '    <a href="javascript:;" onclick="NoticeList.delete('+ item.id +')">删除</a></td>' +
                            ' </tr>'
                    });
					$('#noticeList').html(noticeListHtml);
					// 初始化1次
					if (pageIndex == 1) {
                        NoticeList.showPage();
                    }
                }
            }
		})
    },
    delete: function (id) {
        if(confirm('确认删除此项吗？')) {
            $.ajax({
                url: NoticeList.data.baseUrl + 'notice/delete',
                type: 'GET',
                // dataType: 'json',
                // contentType: 'application/json',
                data: {id: id},
                success: function (response) {
                	if (response.type == 'success') {
                        $('#'+id).remove();
                    } else {
                		alert(response.message);
					}
                }
            });

        }
    },
	// 显示分页按钮
	showPage: function () {
		// 确保组件加载完成
        laypage.render({
            elem: 'pagination'
            ,count: NoticeList.data.totalData //数据总数，从服务端得到
			,limit: NoticeList.data.lineSize
            ,theme: '#337ab7' // 主题色
            ,jump: function(obj, first){
                //obj包含了当前分页的所有参数，比如：
                console.log(obj.curr); //得到当前页，以便向服务端请求对应页的数据。
                console.log(obj.limit); //得到每页显示的条数
                //首次不执行
                if(!first){
                    NoticeList.page(obj.curr);
                }
            }
        });
    }
}