var laypage;
var OrderList = {
	data: {
		baseUrl: '',
		lineSize: 8,
		totalPage: 1,
        totalData: 1
	},
	init: function(baseUrl, a) {
        Main.dateFormat();
        laypage = a;
		OrderList.data.baseUrl = baseUrl;
		OrderList.select();
		// 绑定click事件
		$('#conditionForm').on('click', '.select', OrderList.select);
	},
    select: function () {
      OrderList.page(1);
    },
	// 分页查询
	page: function (pageIndex) {
		// 获取查询条件
		var params = {};
        var auditing = $('#conditionForm select[name=auditing]').val();
        params.auditing = auditing;

        // 初始化data
		var data={
            pageIndex: pageIndex,
            lineSize: OrderList.data.lineSize,
            params: params
		};
		// ajax post
		$.ajax({
			url: OrderList.data.baseUrl + 'order/page',
			type: 'POST',
            dataType: 'json',
            contentType: 'application/json',
			data: JSON.stringify(data),
            success: function (response) {
				if(response.type == 'success') {
                    var page = response.data;
                    // 同步总页数
                    OrderList.data.totalPage = page.totalPageCount;
                    // 总条数
					OrderList.data.totalData = page.totalDataCount;
					var data = page.data;
					var OrderListHtml = '';
					$.each(data, function (index, item) {
                        OrderListHtml += '<tr id="'+ item.id +'">' +
                            '<td>'+ item.id +'</td>' +
                            '<td>'+ item.userName +'</td>' +
                            '<td>'+ item.brand +'</td>' +
                            '<td>'+ item.renprice +'</td>' +
                            '<td>'+ item.renday +'</td>' +
                            '<td>'+ item.deposit +'</td>' +
                            '<td>'+ item.totalprice +'</td>' +
                            '<td>'+ (item.paymethod == 1 ? '在线支付' : '门店支付') +'</td>' +
                            '<td>'+ (item.givestatus == 1 ? '已还车' : '未还车') +'</td>' +
                            '<td>'+ new Date(item.createtime).format('yyyy-MM-dd HH:mm:ss') +'</td>' +
                            '<td>'+ (item.auditing) +'</td>' +
                            '<td><a href="javascript:;" onclick="OrderList.checkOrder('+ item.id +')">审核</a>' +
                            // '    <a href="'+ OrderList.data.baseUrl +'order/look?id='+ item.id +'">编辑&nbsp;</a>' +
                            '    <a href="javascript:;" onclick="OrderList.delete('+ item.id +')">删除</a></>' +
                            ' </tr>'
                    });
					$('#orderList').html(OrderListHtml);
					// 初始化1次
					if (pageIndex == 1) {
                        OrderList.showPage();
                    }
                }
            }
		})
    },
    delete: function (id) {
        if(confirm('确认删除此项吗？')) {
            $.ajax({
                url: OrderList.data.baseUrl + 'order/delete',
                type: 'GET',
                // dataType: 'json',
                // contentType: 'application/json',
                data: {id: id},
                success: function (response) {
                	if (response.type == 'success') {
                        $('#'+id).remove();
                    } else {
                		alert(response.message);
					}
                }
            });

        }
    },
	// 显示分页按钮
	showPage: function () {
		// 确保组件加载完成
        laypage.render({
            elem: 'pagination'
            ,count: OrderList.data.totalData //数据总数，从服务端得到
			,limit: OrderList.data.lineSize
            ,theme: '#337ab7' // 主题色
            ,jump: function(obj, first){
                //obj包含了当前分页的所有参数，比如：
                // console.log(obj.curr); //得到当前页，以便向服务端请求对应页的数据。
                // console.log(obj.limit); //得到每页显示的条数
                //首次不执行
                if(!first){
                    OrderList.page(obj.curr);
                }
            }
        });
    },
    checkOrder: function (id) {
        if (confirm('是否通过此订单？')) {
            $.ajax({
                url: OrderList.data.baseUrl + 'order/check',
                type: 'GET',
                data: {orderId: id},
                success: function (response) {
                    if (response.type == 'success') {
                        alert(response.message);
                        OrderList.select();
                    } else {
                        alert(response.message);
                    }
                }
            });
        }
    }
}