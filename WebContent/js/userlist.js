var laypage;
var UserList = {
	data: {
		baseUrl: '',
		lineSize: 8,
		totalPage: 1,
        totalData: 1
	},
	init: function(baseUrl, a) {
        laypage = a;
		UserList.data.baseUrl = baseUrl;
		UserList.page(1);
		// 绑定click事件
		$('#conditionForm').on('click', '.chaxun', UserList.select);
	},
    select: function () {
    	UserList.page(1);
    },
	// 分页查询
	page: function (pageIndex) {
		// 获取查询条件
		var params = {};
        var id = $('#conditionForm input[name=id]').val();
        var username = $('#conditionForm input[name=username]').val();
        var role = $('#conditionForm select[name=role]').val();
        params.id = id;
        params.username = username;
        params.role = role;
       

        // 初始化data
		var data={
            pageIndex: pageIndex,
            lineSize: UserList.data.lineSize,
            params: params
		};
		// ajax post
		$.ajax({
			url: UserList.data.baseUrl + 'user/page',
			type: 'POST',
            dataType: 'json',
            contentType: 'application/json',
			data: JSON.stringify(data),
            success: function (response) {
				if(response.type == 'success') {
                    console.log(response.message);
                    var page = response.data;
                    // 同步总页数
                    UserList.data.totalPage = page.totalPageCount;
                    // 总条数
					UserList.data.totalData = page.totalDataCount;
					var data = page.data;
					var userListHtml = '';
					$.each(data, function (index, item) {
                        userListHtml += '<tr id="'+ item.id +'">' +
                            '<td>'+ item.id +'</td>' +
                            '<td>'+ item.username +'</td>' +
                            '<td>'+ item.identity +'</td>' +
                            '<td>'+ item.concat +'</td>' +
                            '<td>'+ item.license +'</td>' +
                            '<td>'+ item.uemail +'</td>' +
                            '<td>'+ item.role +'</td>' +
                            '<td><a href="'+ UserList.data.baseUrl +'user/look?id='+ item.id +'">编辑&nbsp;</a>' +
                            '    <a href="javascript:;" onclick="UserList.delUser('+ item.id +')">删除</a></td>' +
                            ' </tr>'
                    });
					$('#userList').html(userListHtml);
					// 初始化1次
					if (pageIndex == 1) {
                        UserList.showPage();
                    }
                }
            }
		})
    },
    delUser: function (id) {
        if(confirm('确认删除此项吗？')) {
            $.ajax({
                url: UserList.data.baseUrl + 'user/delete',
                type: 'GET',
                // dataType: 'json',
                // contentType: 'application/json',
                data: {id: id},
                success: function (response) {
                	if (response.type == 'success') {
                        $('#'+id).remove();
                    } else {
                		alert(response.message);
					}
                }
            });

        }
    },
	// 显示分页按钮
	showPage: function () {
		// 确保组件加载完成
        laypage.render({
            elem: 'pagination'
            ,count: UserList.data.totalData //数据总数，从服务端得到
			,limit: UserList.data.lineSize
            ,theme: '#337ab7' // 主题色
            ,jump: function(obj, first){
                //obj包含了当前分页的所有参数，比如：
                console.log(obj.curr); //得到当前页，以便向服务端请求对应页的数据。
                console.log(obj.limit); //得到每页显示的条数
                //首次不执行
                if(!first){
                    UserList.page(obj.curr);
                }
            }
        });
    }
}