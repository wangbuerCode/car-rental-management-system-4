var UserLook = {
    data: {
        baseUrl: ''
    },
	init: function(baseUrl) {
        UserLook.data.baseUrl = baseUrl;
        $('#userForm a.update').on('click', UserLook.updateUser);

	},
    updateUser: function () {
        var id = $('#userForm input[name=id]').val();
        var username = $('#userForm input[name=username]').val();
        var password = $('#userForm input[name=password]').val();
        var identity = $('#userForm input[name=identity]').val();
        var concat = $('#userForm input[name=concat]').val();
        var license = $('#userForm input[name=license]').val();
        var uemail = $('#userForm input[name=uemail]').val();
        var role = $('#userForm input[name=role]').val();
        if(!(id || username || password || identity || concat || role)) {
            alert("表单内容有误");
            return;
        }
        var data = {};
        data.id = id;
        data.username = username;
        data.password = password;
        data.identity = identity;
        data.concat = concat;
        data.license = license;
        data.uemail = uemail;
        data.role = role;

        $.ajax({
            url: UserLook.data.baseUrl + 'user/update',
            type: 'POST',
            dataType: 'json',
            contentType: 'application/json',
            data: JSON.stringify(data),
            success: function (response) {
                if (response.type == 'success') {
                    window.location.href = UserLook.data.baseUrl + '/user/list';
                } else {
                    alert(response.message);
                }
            }
        });
    }
}