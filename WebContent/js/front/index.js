var laypage, laydate;
var Index = {
    data: {
    	baseUrl: '',
		lineSize: 8,
		totalPage: 1,
        totalData: 1
    },
	init: function(baseUrl, a, b) {
		laypage = a;
		laydate = b;
        Index.data.baseUrl = baseUrl;
        Main.showShop();
        $('#district1, #province1, #city1').change(Main.showShop);
        // 绑定click事件
		$('#conditionForm').on('click', '.chaxun', Index.select);
		//类型获取
		$('#pagelet-CarLevelList').on('click', 'a', Index.levelClick);
		//品牌选择获取 给每个a标签绑定时间
		$('#pagelet-BrandList').on('click', 'a', Index.brandClick);
		// 初始化加载
		Index.select();
		//给预订按钮添加请求url
		$('#reservationList').on('click','.licar-btn ',Index.order);
        //日期时间选择器
        laydate.render({
            elem: '#pickdate'
            ,type: 'datetime'
            ,min: 0
            ,format: 'yyyy/MM/dd HH:mm'
            ,theme: '#393D49'
            ,range: true
            ,done: function(value, date, endDate){
                var pickdate = new Date(date.year, date.month - 1, date.date, date.hours, date.minutes);
                var stilldate = new Date(endDate.year, endDate.month - 1, endDate.date, endDate.hours, endDate.minutes);
                $('#startTime').val(pickdate.getTime());
                $('#endTime').val(stilldate.getTime());
            }
        });
	},
	levelClick: function () {
		$('#pagelet-CarLevelList a.on').removeClass('on');
		$(this).addClass('on');
		Index.select();
	},
	brandClick: function () {
		$('#pagelet-BrandList a.on').removeClass('on');
		$(this).addClass('on');
		Index.select();
	},
	/*priceClick:function(){
		$('#pricelist a.on').removeClass('on');
		$(this).addClass('on');
		Index.select();
	},*/
	//预订跳转
	order:function(){
		var id = $(this).attr('data-cid');
		var pickdate = $('#startTime').val();
		var stilldate = $('#endTime').val();
		if (!pickdate || !stilldate){
            alert("请选择取车、还车时间");
        } else {
            window.location.href = Index.data.baseUrl + 'front/ordercar?id='+ id +'&pickdate='+ pickdate +'&stilldate='+ stilldate;
		}
	},
	select: function() {
		Index.page(1);
	},
	// 分页查询
	page: function (pageIndex) {
		// 获取查询条件
		var params = {};
        var province = $('#conditionForm select[name=province]').val();
        var city = $('#conditionForm select[name=city]').val();
        var area = $('#conditionForm select[name=area]').val();
        var jtstore = $('#conditionForm select[name=jtstore]').val();
        var type = $('#pagelet-CarLevelList a.on').attr('data-value');
        var brand = $('#pagelet-BrandList a.on').attr('data-value');

        params.province = province;
        params.city = city;
        params.area = area;
        params.jtstore = jtstore;
        params.type = type;
        params.brand = brand;
        
        // 初始化data
		var data={
            pageIndex: pageIndex,
            lineSize: Index.data.lineSize,
            params: params
		};
		// ajax post
		$.ajax({
			url: Index.data.baseUrl + 'car/userPage',
			type: 'POST',
            dataType: 'json',
            contentType: 'application/json',
			data: JSON.stringify(data),
            success: function (response) {
				if(response.type == 'success') {
                    console.log(response.message);
                    // 得到数据
                    var page = response.data;
                    // 同步总页数
                    Index.data.totalPage = page.totalPageCount;
                    // 总条数
					Index.data.totalData = page.totalDataCount;
					var indexHtml = '';
					if(Index.data.totalData == 0){
						$('#nocar').show();
						$('#pagination').hide();
					} else {
						$('#nocar').hide();
						var data = page.data;
						$.each(data, function (index, item) {
	                        /*indexHtml += '<tr id="'+ item.id +'">' +
	                            '<td>'+ item.id +'</td>' +
	                            '<td>'+ item.type +'</td>' +
	                            '<td>'+ item.brand +'</td>' +
	                            '<td>'+ item.renprice +'</td>' +
	                            '<td>'+ item.deposit +'</td>' +
	                            '<td>'+ item.peccancy +'</td>' +
	                            '<td>'+ item.province +''+item.city +''+item.area +'</td>' +
	                            '<td>'+ item.jtstore +'</td>' +
	                            '<td>'+ item.renstatus +'</td>' +
	                            '<td>'+ item.carstatus +'</td>' +
	                            '<td><a href="'+ Index.data.baseUrl +'car/look?id='+ item.id +'">编辑&nbsp;</a>' +
	                            '    <a href="javascript:;" onclick="Index.delCar('+ item.id +')">删除</a></td>' +
	                            ' </tr>';*/
							indexHtml += '<div class="det-carlist" style="z-index: 299;">\n' +
				            '\t\t\t\t\t\t\t<ul class="clearfix">\n' +
				            '\t\t\t\t\t\t<li class="licar-pic">\n' +
				            '\t\t\t\t\t\t\t<a href="javascript:void(0)"\n' +
				            '\t\t\t\t\t\t\tclass="car-pic" data-iscargourp="True" data-cid="564"> \n' +
				            '\t\t\t\t\t\t\t<img src="'+Index.data.baseUrl+item.tupian+'"\n' +
				            '\t\t\t\t\t\t\t\tclass="lazy" alt="标致3008">\n' +
				            '\t\t\t\t\t\t\t</a>\n' +
				            '\t\t\t\t\t\t</li>\n' +
				            '\t\t\t\t\t\t<li class="licar-name">\n' +
				            '\t\t\t\t\t\t\t<div class="namesub">\n' +
				            '\t\t\t\t\t\t\t\t<div class="namecontent">\n' +
				            '\t\t\t\t\t\t\t\t\t<p class="car-nameinfo">\n' +
				            '\t\t\t\t\t\t\t\t\t\t<span>'+item.brand +'或同组车型</span> <a href="javascript:void(0);"\n' +
				            '\t\t\t\t\t\t\t\t\t\t\tclass="sp-aczs sp-box"><i class="lv5"></i></a> <span\n' +
				            '\t\t\t\t\t\t\t\t\t\t\tclass="rebate">返</span>\n' +
				            '\t\t\t\t\t\t\t\t\t</p>\n' +
				            '\t\t\t\t\t\t\t\t\t<p class="car-distribute">\n' +
				            '\t\t\t\t\t\t\t\t\t\t【随机分配如大众斯柯达野帝、Jeep自由侠、标致3008或类似车型】</p>\n' +
				            '\t\t\t\t\t\t\t\t\t<p class="car-introinfo">\n' +
				            '\t\t\t\t\t\t\t\t\t\t<span>'+item.type+'|自动|5座</span>\n' +
				            '\t\t\t\t\t\t\t\t\t</p>\n' +
				            '\t\t\t\t\t\t\t\t\t<p class="car-typeinfo">\n' +
				            '\t\t\t\t\t\t\t\t\t\t<span></span>\n' +
				            '\t\t\t\t\t\t\t\t\t</p>\n' +
				            '\t\t\t\t\t\t\t\t</div>\n' +
				            '\t\t\t\t\t\t\t</div>\n' +
				            '\t\t\t\t\t\t</li>\n' +
				            '\n' +
				            '\t\t\t\t\t\t<li class="licar-info">\n' +
				            '\t\t\t\t\t\t\t<p class="condition1">\n' +
				            '\t\t\t\t\t\t\t\t<!--标准价-->\n' +
				            '\t\t\t\t\t\t\t\t<span class="cartitle"> <i\n' +
				            '\t\t\t\t\t\t\t\t\tclass="sp-box discount icon-ticket">惠</i>活动促销价\n' +
				            '\t\t\t\t\t\t\t\t</span> <span class="carprice"> <i class="symbol">¥</i> <em\n' +
				            '\t\t\t\t\t\t\t\t\tclass="total-price">'+item.renprice+'</em>/日均\n' +
				            '\t\t\t\t\t\t\t\t</span> <span class="licar-btn btnopen1" data-cid="'+ item.id +'">预订</span> <span\n' +
				            '\t\t\t\t\t\t\t\t\tclass="typebtn btntop1 "> <a href="javascript:;"\n' +
				            '\t\t\t\t\t\t\t\t\tclass="type-four type-fourone btn-book" data-price-type="7"\n' +
				            '\t\t\t\t\t\t\t\t\tdata-cid="4BLP0QpWI10=" data-gid="kQSEOmxNcx0="> <span>\n' +
				            '\t\t\t\t\t\t\t\t\t\t\t闪租价<br>\n' +
				            '\t\t\t\t\t\t\t\t\t</span><i class="symbol">¥</i> <em class="total-price">191</em>\n' +
				            '\t\t\t\t\t\t\t\t</a> \n' +
				            '\t\t\t\t\t\t\t\t<a href="javascript:;"\n' +
				            '\t\t\t\t\t\t\t\t\tclass="type-four type-fourone btn-book" data-price-type="1"\n' +
				            '\t\t\t\t\t\t\t\t\tdata-cid="4BLP0QpWI10=" data-gid="kQSEOmxNcx0="> <span>\n' +
				            '\t\t\t\t\t\t\t\t\t\t\t门店现付<br>\n' +
				            '\t\t\t\t\t\t\t\t\t</span><i class="symbol">¥</i> <em class="total-price">126</em>\n' +
				            '\t\t\t\t\t\t\t\t</a> \n' +
				            '\t\t\t\t\t\t\t\t<a href="javascript:;"\n' +
				            '\t\t\t\t\t\t\t\t\tclass="type-four type-fourtwo btn-book" data-price-type="5"\n' +
				            '\t\t\t\t\t\t\t\t\tdata-cid="4BLP0QpWI10=" data-gid="kQSEOmxNcx0="> <span>\n' +
				            '\t\t\t\t\t\t\t\t\t\t\t返现价<br> <em>138元返现12元/天</em>\n' +
				            '\t\t\t\t\t\t\t\t\t</span><i class="symbol">¥</i> <em class="total-price">126</em>\n' +
				            '\t\t\t\t\t\t\t\t</a> \n' +
				            '\t\t\t\t\t\t\t\t<a href="javascript:;"\n' +
				            '\t\t\t\t\t\t\t\t\tclass="type-four type-fourtwo notchoose" data-price-type="3"\n' +
				            '\t\t\t\t\t\t\t\t\tdata-cid="4BLP0QpWI10=" data-gid="kQSEOmxNcx0="> <span>\n' +
				            '\t\t\t\t\t\t\t\t\t\t\t在线预付<br> <em>需要提前3天预订</em>\n' +
				            '\t\t\t\t\t\t\t\t\t</span><i class="symbol">¥</i> <em class="total-price">124</em>\n' +
				            '\t\t\t\t\t\t\t\t</a>\n' +
				            '\t\t\t\t\t\t\t\t</span>\n' +
				            '\t\t\t\t\t\t\t</p>\n' +
				            '\t\t\t\t\t\t</li>\n' +
				            '\t\t\t\t\t</ul>\n' +
				            '\t\t\t\t</div>';
	                    });
					}
					$('#reservationList').html(indexHtml);
					// 初始化1次
					if (pageIndex == 1) {
                        Index.showPage();
                    }
                }
            }
		})
    },
	
	// 显示分页按钮
	showPage: function () {
		// 确保组件加载完成
        laypage.render({
            elem: 'pagination'
            ,count: Index.data.totalData //数据总数，从服务端得到
			,limit: Index.data.lineSize
            ,theme: '#337ab7' // 主题色
            ,jump: function(obj, first){
                //obj包含了当前分页的所有参数，比如：
                console.log(obj.curr); //得到当前页，以便向服务端请求对应页的数据。
                console.log(obj.limit); //得到每页显示的条数
                //首次不执行
                if(!first){
                    Index.page(obj.curr);
                }
            }
        });
    }
}