var Order = {
	data : {
		baseUrl : ''
		,carid: ''
		,pickdate: ''
		,stilldate: ''
	},
	init : function(baseUrl, a) {
        Main.dateFormat();
	    Order.data.baseUrl = baseUrl;
		Order.showTime();
		Order.totalPrice();
		$('.right input[name=commitOrder]').on('click', Order.commitOrder);
	},
	showTime: function(){
		var params = Main.getRequest();
        Order.data.carid = params.id;
        Order.data.pickdate = params.pickdate;
        Order.data.stilldate = params.stilldate;

        var pickdate = new Date(parseInt(params.pickdate)).format("yyyy-MM-dd HH:mm");
        var stilldate = new Date(parseInt(params.stilldate)).format("yyyy-MM-dd HH:mm");
		$('.pickdate span').html(pickdate);
        $('.stilldate span').html(stilldate);
        $('.orderpart .left-img p span').text(Order.getDays(parseInt(params.pickdate), parseInt(params.stilldate)));
    },
    //计算租车时间
	getDays: function (startDateTime, endDateTime){
		var days = parseFloat((endDateTime - startDateTime) / (1000 * 60 * 60 * 24)).toFixed(1);
		return days;
	},
	//计算订单总金额
    totalPrice: function () {
		var renprice = parseInt($('.right ul .renprice').text());
		var serviceprice = parseInt($('.right ul .serviceprice').text());
		var deposit = parseInt($('.right ul .deposit').text());
        var days = parseInt($('.orderpart .left-img p span').text());
        var total = renprice * days + deposit + serviceprice;
		$('.right ul .total').text(total);
    },
	commitOrder: function () {
		var params = {};
		params.carid = Order.data.carid;
		params.renday = $('.orderpart .left-img p span').text();
		params.totalprice = $('.right ul .total').text();
		params.pickdate = Order.data.pickdate;
		params.stilldate = Order.data.stilldate;
		params.paymethod = 1;

		$.ajax({
            url: Order.data.baseUrl + 'order/insert',
            type: 'POST',
            dataType: 'json',
            contentType: 'application/json',
            data: JSON.stringify(params),
            success: function (response) {
                if (response.type == 'success') {
                    alert(response.message)
					window.location.href = Order.data.baseUrl;
                } else {
                    alert(response.message)
				}
            }
        });
    }
}