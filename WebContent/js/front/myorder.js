var laypage;
var MyOrder = {
	data: {
		baseUrl: '',
		lineSize: 8,
		totalPage: 1,
        totalData: 1,
        params: {
        	auditing: '',
        	givestatus: ''
        }
	},
	init: function(baseUrl, a) {
        laypage = a;
        Main.dateFormat();
        MyOrder.data.baseUrl = baseUrl;
        MyOrder.page(1);
		// 绑定click事件
		$('.dingdan').on('click', '.dingdan-title span a', MyOrder.select);
		$('#orderList').on('click', '.cancel', MyOrder.cancelOrder);
		$('#orderList').on('click', '.returnCar', MyOrder.returnCar);
	},
    select: function () {
    	$('.dingdan-title span a').removeClass('on');
    	$(this).addClass('on');
    	var value = $(this).attr('data-value');
    	var auditing = '', givestatus = '';
    	switch(value) {
	    	case 'all': break;
	    	case 'yy': auditing = '待审核'; break;
	    	case 'zl': auditing = '审核通过',givestatus = '0' ; break;
	    	case 'wc': givestatus = '1'; break;
    	}
    	MyOrder.data.params.auditing = auditing;
    	MyOrder.data.params.givestatus = givestatus;
    	MyOrder.page(1);
    },
	// 分页查询
	page: function (pageIndex) {
		// 获取查询条件
		var params = MyOrder.data.params;
        // 初始化data
		var data={
            pageIndex: pageIndex,
            lineSize: MyOrder.data.lineSize,
            params: params
		};
		// ajax post
		$.ajax({
			url: MyOrder.data.baseUrl + 'order/myOrderPage',
			type: 'POST',
            dataType: 'json',
            contentType: 'application/json',
			data: JSON.stringify(data),
            success: function (response) {
				if(response.type == 'success') {
                    var page = response.data;
                    // 同步总页数
                    MyOrder.data.totalPage = page.totalPageCount;
                    // 总条数
					MyOrder.data.totalData = page.totalDataCount;
					var data = page.data;
					var MyOrderHtml = '';
					$.each(data, function (index, item) {
                        MyOrderHtml += '<tr>' +
                        	'<th>'+ item.brand +'</th>' +
    						'<th>'+ item.jtstore +'</th>' +
    						'<th>'+ item.totalprice +'</th>' +
    						'<th class="fine" data-value="'+ item.fine +'">'+ (item.fine?item.fine:'0') +'</th>' +
    						'<th>'+ new Date(item.pickdate).format('yyyy-MM-dd HH:mm') +'</th>' +
    						'<th>'+ new Date(item.stilldate).format('yyyy-MM-dd HH:mm') +'</th>' +
    						'<th>'+ item.auditing +'</th>' +
    						'<th data-value="'+ item.id +'">'+ 
    						(item.auditing=='待审核' ? '<span class=\"btn btn-sm btn-default cancel\">取消订单</span>' : 
    							(item.givestatus? "已还车" : "<span class=\"btn btn-sm btn-default returnCar\">还 车</span>")) +
    						'</th>' +
    					'</tr>';
                    });
					$('#orderList').html(MyOrderHtml);
					// 初始化1次
					if (pageIndex == 1) {
                        MyOrder.showPage();
                    }
                } else {
                	alert(response.message);
                }
            }
		})
    },
	// 显示分页按钮
	showPage: function () {
		// 确保组件加载完成
        laypage.render({
            elem: 'pagination'
            ,count: MyOrder.data.totalData //数据总数，从服务端得到
			,limit: MyOrder.data.lineSize
            ,theme: '#337ab7' // 主题色
            ,jump: function(obj, first){
                //obj包含了当前分页的所有参数，比如：
                // console.log(obj.curr); //得到当前页，以便向服务端请求对应页的数据。
                // console.log(obj.limit); //得到每页显示的条数
                //首次不执行
                if(!first){
                    MyOrder.page(obj.curr);
                }
            }
        });
    },
    // 取消订单
    cancelOrder: function() {
    	if(!confirm('确认取消预定吗')) {
    		return;
    	}
    	var data = {};
    	var dom =  $(this);
    	data.id = dom.parent().attr('data-value');
    	if(!data.id) {
    		alert('参数错误，请刷新后重试');
    	}
    	// console.log(data);
    	$.ajax({
			url: MyOrder.data.baseUrl + 'order/delete',
			type: 'GET',
			data: data,
            success: function (response) {
				if(response.type == 'success') {
					alert('取消成功');
					dom.parents('tr').remove();
				}
            }
    	});
    },
    // 还车
    returnCar: function() { 
    	var dom =  $(this);
    	var fine = dom.parents('tr').find('th.fine').attr('data-value');
    	if(fine > 0) {
    		alert('请支付违约金');
    	}
    	var data = {};
    	data.id = dom.parent().attr('data-value');
    	data.givestatus = '1';
    	//    	console.log(data);
    	$.ajax({
			url: MyOrder.data.baseUrl + 'order/update',
			type: 'POST',
            dataType: 'json',
            contentType: 'application/json',
			data: JSON.stringify(data),
            success: function (response) {
				if(response.type == 'success') {
					alert('还车成功');
			    	dom.parent().html('已还车');
				}
            }
    	});
    }
}