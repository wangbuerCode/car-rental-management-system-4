var laypage;
var CarList = {
	data: {
		baseUrl: '',
		lineSize: 8,
		totalPage: 1,
        totalData: 1
	},
	init: function(baseUrl, a) {
        laypage = a;
        // 将省市区设为默认未选中状态
        $('#distpicker').distpicker('reset', true);
		CarList.data.baseUrl = baseUrl;
		CarList.page(1);
		// 绑定click事件
		$('#conditionForm').on('click', '.chaxun', CarList.select);
	},
    select: function () {
      CarList.page(1);
    },
	// 分页查询
	page: function (pageIndex) {
		// 获取查询条件
		var params = {};
        var type = $('#conditionForm select[name=type]').val();
        var brand = $('#conditionForm select[name=brand]').val();
        var renstatus = $('#conditionForm select[name=renstatus]').val();
        var province = $('#conditionForm select[id=province]').val();
        var city = $('#conditionForm select[id=city]').val();
        var district = $('#conditionForm select[id=district]').val();
        params.type = type;
        params.brand = brand;
        params.renstatus = renstatus;
        params.province = province;
        params.city = city;
        params.area = district;

        // 初始化data
		var data={
            pageIndex: pageIndex,
            lineSize: CarList.data.lineSize,
            params: params
		};
		// ajax post
		$.ajax({
			url: CarList.data.baseUrl + 'car/page',
			type: 'POST',
            dataType: 'json',
            contentType: 'application/json',
			data: JSON.stringify(data),
            success: function (response) {
				if(response.type == 'success') {
                    console.log(response.message);
                    // 得到数据
                    var page = response.data;
                    // 同步总页数
                    CarList.data.totalPage = page.totalPageCount;
                    // 总条数
					CarList.data.totalData = page.totalDataCount;
					var data = page.data;
					var carListHtml = '';
					$.each(data, function (index, item) {
                        carListHtml += '<tr id="'+ item.id +'">' +
                            '<td>'+ item.id +'</td>' +
                            '<td>'+ item.type +'</td>' +
                            '<td>'+ item.brand +'</td>' +
                            '<td>'+ item.renprice +'</td>' +
                            '<td>'+ item.deposit +'</td>' +
                            '<td>'+ item.peccancy +'</td>' +
                            '<td>'+ item.province +''+item.city +''+item.area +'</td>' +
                            '<td>'+ item.jtstore +'</td>' +
                            '<td>'+ item.renstatus +'</td>' +
                            '<td>'+ item.carstatus +'</td>' +
                            '<td><a href="'+ CarList.data.baseUrl +'car/look?id='+ item.id +'">编辑&nbsp;</a>' +
                            '    <a href="javascript:;" onclick="CarList.delCar('+ item.id +')">删除</a></td>' +
                            ' </tr>'
                    });
					$('#carList').html(carListHtml);
					// 初始化1次
					if (pageIndex == 1) {
                        CarList.showPage();
                    }
                }
            }
		})
    },
	delCar: function (id) {
        if(confirm('确认删除此项吗？')) {
            $.ajax({
                url: CarList.data.baseUrl + 'car/delete',
                type: 'GET',
                // dataType: 'json',
                // contentType: 'application/json',
                data: {id: id},
                success: function (response) {
                	if (response.type == 'success') {
                        $('#'+id).remove();
                    } else {
                		alert(response.message);
					}
                }
            });

        }
    },
	// 显示分页按钮
	showPage: function () {
		// 确保组件加载完成
        laypage.render({
            elem: 'pagination'
            ,count: CarList.data.totalData //数据总数，从服务端得到
			,limit: CarList.data.lineSize
            ,theme: '#337ab7' // 主题色
            ,jump: function(obj, first){
                //obj包含了当前分页的所有参数，比如：
                console.log(obj.curr); //得到当前页，以便向服务端请求对应页的数据。
                console.log(obj.limit); //得到每页显示的条数
                //首次不执行
                if(!first){
                    CarList.page(obj.curr);
                }
            }
        });
    }
}