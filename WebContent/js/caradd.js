var CarAdd = {
	data: {
		baseUrl: ''
    },
	init: function(baseUrl) {
		CarAdd.data.baseUrl = baseUrl;
        Main.showShop();
		// 绑定click事件
		$('#conditionForm').on('click', '.addCar', CarAdd.addCar);
        $('#district1, #province1, #city1').change(Main.showShop);
	},
	addCar: function () {
		var type = $('#conditionForm select[name=type]').val();
        var brand = $('#conditionForm select[name=brand]').val();

        var renprice = $('#conditionForm input[name=renprice]').val();
        var deposit = $('#conditionForm input[name=deposit]').val();
        var peccancy = $('#conditionForm input[name=peccancy]').val();
        if (!(renprice && deposit && peccancy)) {
            alert("表单内容有误");
            return;
        }
        // 省市区
        var province = $('#conditionForm select[name=province]').val();
        var city = $('#conditionForm select[name=city]').val();
        var area = $('#conditionForm select[name=area]').val();

        var jtstore = $('#conditionForm select[name=jtstore]').val();
        var renstatus = $('#conditionForm select[name=renstatus]').val();
        var carstatus = $('#conditionForm select[name=carstatus]').val();

        var data = {};
        data.type = type;
        data.brand = brand;

        data.renprice = renprice;
        data.deposit = deposit;
        data.peccancy = peccancy;

        data.province = province;
        data.city = city;
        data.area = area;

        data.jtstore = jtstore;
        data.renstatus = renstatus;
        data.carstatus = carstatus;

        $.ajax({
            url: CarAdd.data.baseUrl + 'car/insert',
            type: 'POST',
            dataType: 'json',
            contentType: 'application/json',
            data: JSON.stringify(data),
            success: function (response) {
                alert(response.message);
                if (response.type == 'success') {
            		window.location.href= CarAdd.data.baseUrl + 'car/list';
				}
            }
        });
    }
	
}