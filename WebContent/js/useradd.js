var UserAdd = {
	data: {
		baseUrl: ''
    },
	init: function(baseUrl) {
		UserAdd.data.baseUrl = baseUrl;
		// 绑定click事件
		$('#addUserForm').on('click', '.addUser', UserAdd.addUser);
	},
    addUser: function () {
		var username = $('#addUserForm input[name=username]').val();
		var password = $('#addUserForm input[name=password]').val();
		var identity = $('#addUserForm input[name=identity]').val();
		var concat = $('#addUserForm input[name=concat]').val();
		var license = $('#addUserForm input[name=license]').val();
		var uemail = $('#addUserForm input[name=uemail]').val();
        var role = $('#addUserForm select[name=role]').val();
        if(!(username && password&&identity&&concat&&license&&uemail&&role)) {
            alert("表单内容有误");
            return;
        }
        var data = {};
        data.username = username;
        data.password = password;
        data.identity = identity;
        data.concat = concat;
        data.license = license;
        data.uemail = uemail;
        data.role = role;

        $.ajax({
            url: UserAdd.data.baseUrl + 'user/insert',
            type: 'POST',
            dataType: 'json',
            contentType: 'application/json',
            data: JSON.stringify(data),
            success: function (response) {
                alert(response.message);
                if (response.type == 'success') {
            		window.location.href= UserAdd.data.baseUrl + 'user/list';
				}
            }
        });
    }
}