var CarLook = {
    data: {
        baseUrl: ''
    },
	init: function(baseUrl, jtstore) {
        CarLook.data.baseUrl = baseUrl;
        Main.showShop(jtstore);
        $('#district1, #province1, #city1').change(Main.showShop);
        $('form .update').on('click', CarLook.updateCar);
	},
    updateCar: function () {
        var id = $('#carForm input[name=id]').val();
        var type = $('#carForm input[name=type]').val();
        var brand = $('#carForm input[name=brand]').val();

        var renprice = $('#carForm input[name=renprice]').val();
        var deposit = $('#carForm input[name=deposit]').val();
        var peccancy = $('#carForm input[name=peccancy]').val();
        if (!(renprice && deposit && peccancy)) {
            alert("表单内容有误");
            return;
        }
        // 省市区
        var province = $('#carForm select[name=province]').val();
        var city = $('#carForm select[name=city]').val();
        var area = $('#carForm select[name=area]').val();
        var jtstore = $('#carForm select[name=jtstore]').val();

        var renstatus = $('#carForm input[name=renstatus]').val();
        var carstatus = $('#carForm input[name=carstatus]').val();

        if (!jtstore) {
            alert("请选择门店");
            return;
        }
        var data = {};
        data.id = id;
        data.type = type;
        data.brand = brand;

        data.renprice = renprice;
        data.deposit = deposit;
        data.peccancy = peccancy;

        data.province = province;
        data.city = city;
        data.area = area;

        data.jtstore = jtstore;
        data.renstatus = renstatus;
        data.carstatus = carstatus;

        $.ajax({
            url: CarLook.data.baseUrl + 'car/update',
            type: 'POST',
            dataType: 'json',
            contentType: 'application/json',
            data: JSON.stringify(data),
            success: function (response) {
                if (response.type == 'success') {
                    window.location.href = CarLook.data.baseUrl + '/car/list';
                } else {
                    alert(response.message);
                }
            }
        });
    }
}