<%@ page language="java" contentType="text/html; charset=utf-8"
         pageEncoding="utf-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
            + path + "/";
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <link rel="stylesheet" href="https://cdn.staticfile.org/twitter-bootstrap/3.3.7/css/bootstrap.min.css">
    <link href="https://cdn.bootcss.com/bootstrap-datepaginator/1.1.0/bootstrap-datepaginator.min.css" rel="stylesheet" >
    <link rel="stylesheet" href="<%=basePath %>assets/layui/css/layui.css">
    <link rel="stylesheet" href="<%=basePath %>css/main.css">
    <title>用户管理</title>
    <style type="text/css">
        .header a {
            color: white;
        }
        
    </style>
</head>
<body>
<div class="header" style="height: 55px;">
    <nav class="navbar navbar-inverse" role="navigation">
        <div class="navbar-inner" style="">
            <div class="navbar-header">
                <a class="navbar-brand" href="#"
                   style="margin-right: 50px; margin-left: 20px;">车辆租赁系统</a>
            </div>
            <div>

                <p class="navbar-text navbar-left">
                    当前时间： <span id="Date"></span>
                </p>

                <!--向右对齐-->
                <p class="navbar-text navbar-right" style="margin-right: 100px;">
                    <a href="<%=basePath %>front/logout">退出</a>
                </p>
                <ul class="nav navbar-nav navbar-right">
                    <li class="dropdown"><a href="#" class="dropdown-toggle"
                                            data-toggle="dropdown"> admin <b class="caret"></b>
                    </a>
                        <ul class="dropdown-menu">
                            <li><a href="#">jmeter</a></li>
                           
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
</div>

<div class="container-fluid">
    <div class="row">
        <!-- 左边查询框 -->
        <div class="left col-lg-2 col-md-2 col-xs-2">
            <ul class="nav nav-pills nav-stacked">
                <li><a href="<%=basePath%>admin/index">首页</a></li>
                <li><a href="<%=basePath%>car/list">汽车管理</a></li>
                <li><a href="<%=basePath%>order/list">订单管理</a></li>
                <li  class="active"><a href="<%=basePath%>user/list">用户管理</a></li>
                <li><a href="<%=basePath%>notice/list">公告管理</a></li>
<%--                 <li><a href="<%=basePath%>user/modifypwd">修改密码</a></li> --%>
            </ul>
        </div>
        <!-- 右边内容部门 -->
        <div class="right col-lg-10 col-md-10 col-xs-10">
	        <div class="search">
	            <form role="form" id="conditionForm" action="<%=basePath%>user/search">
	               工号:
	                <input type="text" name="id">
	                姓名:
	                <input type="text" name="username">
	               角色 :		<select name="role">
	               		<option value="">请选择</option>
	               		<option value="管理员">管理员</option>
	               		<option value="用户">用户</option>
	               </select>
	               <button type="button" class="chaxun">查询</button>
	               <button class="add">
	                    <a href="<%=basePath%>user/toAdd" style="color: white;">添加</a>
	               </button>
	            </form>
	        </div>
	       
	        <div style="padding-left: 10px;">
	            <table class="table table-hover">
	                <caption></caption>
	                <thead>
	                    <tr>
	                        <th>id</th>
                            <th>姓名</th>
                            <th>身份证号</th>
                            <th>联系方式</th>
                            <th>驾驶证号</th>
                            <th>邮箱</th>
                            <th>角色</th>
                            <th>操作</th>
	                    </tr>
	                </thead>
	                <tbody id="userList"></tbody>
	            </table>
	        </div>
	        <div class="text-center" id="pagination"></div>
    	</div>
	</div>
</div>
<script src="https://cdn.staticfile.org/jquery/2.1.1/jquery.min.js"></script>
<script src="https://cdn.staticfile.org/twitter-bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script src="<%=basePath %>assets/layui/layui.js"></script>
<script src="<%=basePath %>js/distpicker.data.js"></script>
<script src="<%=basePath %>js/distpicker.js"></script>
<script src="<%=basePath %>js/main.js"></script>
<script src="<%=basePath %>js/userlist.js"></script>
<script type="text/javascript">
    $(function () {
        // 引入layui的laypage模块
        layui.use('laypage', function(){
            var laypage = layui.laypage;
            // 初始化
            UserList.init('<%=basePath %>', laypage);
        });
    });
</script>
</body>
</html>