<%@ page language="java" contentType="text/html; charset=utf-8"
         pageEncoding="utf-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
            + path + "/";
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <link href="http://www.jq22.com/jquery/bootstrap-3.3.4.css"
          rel="stylesheet">
    <!--[if IE]>
    <script src="http://libs.baidu.com/html5shiv/3.7/html5shiv.min.js"></script>
    <![endif]-->
	<link rel="stylesheet" href="https://cdn.staticfile.org/twitter-bootstrap/3.3.7/css/bootstrap.min.css">
	<link rel="stylesheet" href="<%=basePath %>assets/layui/css/layui.css">
	<link rel="stylesheet" href="<%=basePath %>/css/main.css">
    <title>用户管理</title>
    <style type="text/css">
        .header a {
            color: white;
        }
        .zuotd {
			width: 150px;
			height: 40px;
			text-align: right;
			padding-right: 20px;
			border: 2px solid grey;
			font-weight: bold;
		}

		.youtd {
			width: 300px;
			height: 40px;
			border: 2px solid grey;
			padding-left: 8px;
		}
		.youtd input{
			height: 30px;
			width: 200px;
		}
    </style>
</head>
<body>
<div class="header" style="height: 55px;">
    <nav class="navbar navbar-inverse" role="navigation">
        <div class="navbar-inner" style="">
            <div class="navbar-header">
                <a class="navbar-brand" href="#" style="margin-right: 50px; margin-left: 20px;">车辆租赁系统</a>
            </div>
        <div>
        <p class="navbar-text navbar-left">
			当前时间： <span id="Date"></span>
        </p>
        <!--向右对齐-->
        <p class="navbar-text navbar-right" style="margin-right: 100px;">
        	<a href="<%=basePath %>front/logout">退出</a>
        </p>
        <ul class="nav navbar-nav navbar-right">
        	<li class="dropdown">
        		<a href="#" class="dropdown-toggle" data-toggle="dropdown"> admin <b class="caret"></b></a>
				<ul class="dropdown-menu">
                	<li><a href="#">jmeter</a></li>
                    
                </ul>
			</li>
        </ul>
    </nav>
</div>

<div class="container-fluid">
    <div class="row">
        <!-- 左边查询框 -->
        <div class="left col-lg-2 col-md-2 col-xs-2">
            <ul class="nav nav-pills nav-stacked">
				<li><a href="<%=basePath%>admin/index">首页</a></li>
                <li><a href="<%=basePath%>car/list">汽车管理</a></li>
				<li><a href="<%=basePath%>order/list">订单管理</a></li>
                <li class="active"><a href="<%=basePath%>user/list">用户管理</a></li>
                <li><a href="<%=basePath%>notice/list">公告管理</a></li>
<%--                 <li><a href="<%=basePath%>user/modifypwd">修改密码</a></li> --%>
            </ul>
        </div>
        <!-- 右边内容部门 -->
        <div class="right col-lg-10 col-md-10 col-xs-10">
        	<div class="looktable">
            	<form  class="form-inline" id="addUserForm" style="margin-top: 60px;">
	                <table>
	                    <tr>
                            <td class="zuotd" colspan="2" style="text-align: center;">添加信息</td>
	                    </tr>
	                    <tr>
	                        <td class="zuotd">用户名:</td>
	                        <td class="youtd" >
								<input type="text" name="username" placeholder="用户名">
	                        </td>
	                    </tr>
	                    <tr>
	                        <td class="zuotd">密码:</td>
	                        <td class="youtd">
								<input type="password" name="password" placeholder="密码" value="">
	                        </td>
	                    </tr>
	                    <tr>
	                        <td class="zuotd">身份证号:</td>
	                        <td class="youtd"><input type="text" name="identity" placeholder="身份证号"></td>
	                    </tr>
	                    <tr>
	                        <td class="zuotd">联系方式:</td>
	                        <td class="youtd"><input type="text" name="concat"  placeholder="联系方式"></td>
	                    </tr>
	                    <tr>
	                        <td class="zuotd">驾驶证号:</td>
	                        <td class="youtd"><input type="text" name="license" placeholder="身份证号"></td>
	                    </tr>
	                    <tr>
	                        <td class="zuotd">邮箱:</td>
	                        <td class="youtd"><input type="text" name="uemail"  placeholder="联系方式"></td>
	                    </tr>
	                    <tr>
	                        <td class="zuotd">角色:</td>
	                        <td class="youtd">
	                        	<select name="role" class="select-100">
	                        		<option value="用户">用户</option>
	                        		<option value="管理员">管理员</option>
	                        	</select>
	                        </td>
	                    </tr>
	                    <tr>
	                        <td colspan="2" class="text-center">
								<br>
								<a href="javascript:;" class="btn btn-primary addUser" role="button">添 加</a>
								<a href="<%=basePath%>user/list" class="btn btn-default" role="button">返 回</a>
	                        </td>
	                    </tr>
	                </table>
            	</form>
        	</div>
    	</div>
    </div>
</div>
<script src="https://cdn.staticfile.org/jquery/2.1.1/jquery.min.js"></script>
<script src="https://cdn.staticfile.org/twitter-bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script src="<%=basePath %>/js/main.js"></script>
<script src="<%=basePath %>/js/useradd.js"></script>
<script type="text/javascript">
    $(function () {
    	UserAdd.init('<%=basePath%>');
    });
</script>
</body>
</html>