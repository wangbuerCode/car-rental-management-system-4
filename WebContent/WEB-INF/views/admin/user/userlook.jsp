<%@ page language="java" contentType="text/html; charset=utf-8"
         pageEncoding="utf-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
            + path + "/";
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <link href="http://www.jq22.com/jquery/bootstrap-3.3.4.css"
          rel="stylesheet">
    <!--[if IE]>
    <script src="http://libs.baidu.com/html5shiv/3.7/html5shiv.min.js"></script>
    <![endif]-->
    <link rel="stylesheet" href="../css/main.css">
    <link rel="stylesheet"
          href="https://cdn.staticfile.org/twitter-bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://cdn.staticfile.org/jquery/2.1.1/jquery.min.js"></script>
    <script
            src="https://cdn.staticfile.org/twitter-bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <title>汽车管理</title>
    <style type="text/css">
        .header a {
            color: white;
        }
         .zuotd {
			width: 150px;
			height: 40px;
			text-align: right;
			padding-right: 20px;
			border: 2px solid grey;
			font-weight: bold;
		}

		.youtd {
			width: 300px;
			height: 40px;
			border: 2px solid grey;
			padding-left: 8px;
		}
		.youtd input{
			height: 30px;
			width: 200px;
		}
    </style>
</head>
<body>
<div class="header" style="height: 55px;">
    <nav class="navbar navbar-inverse" role="navigation">
        <div class="navbar-inner" style="">
            <div class="navbar-header">
                <a class="navbar-brand" href="#" style="margin-right: 50px; margin-left: 20px;">车辆租赁系统</a>
            </div>
            <div>
                <p class="navbar-text navbar-left">
                   	 当前时间： <span id="Date"></span>
                </p>
                <!--向右对齐-->
                <p class="navbar-text navbar-right" style="margin-right: 100px;">
                    <a href="<%=basePath %>front/logout">退出</a>
                </p>
                <ul class="nav navbar-nav navbar-right">
                    <li class="dropdown">
                    	<a href="#" class="dropdown-toggle" data-toggle="dropdown"> admin <b class="caret"></b></a>
                        <ul class="dropdown-menu">
                            <li><a href="#">jmeter</a></li>
                        </ul>
                    </li>
                </ul>
                <!-- <form class="navbar-form navbar-right" role="search"> -->
                <!-- 	<button type="submit" class="btn btn-default">向右对齐-提交按钮</button> -->
                <!-- </form> -->
            </div>
        </div>
    </nav>
</div>
<div class="container-fluid">
    <div class="row">
        <!-- 左边查询框 -->
        <div class="left col-lg-2 col-md-2 col-xs-2">
            <ul class="nav nav-pills nav-stacked">
                <li><a href="<%=basePath%>admin/index">首页</a></li>
                <li><a href="<%=basePath%>car/list">汽车管理</a></li>
                <li><a href="<%=basePath%>order/list">订单管理</a></li>
                <li class="active"><a href="<%=basePath%>user/list">用户管理</a></li>
                 <li><a href="<%=basePath%>notice/list">公告管理</a></li>
<%--                 <li><a href="<%=basePath%>user/modifypwd">修改密码</a></li> --%>
            </ul>
        </div>
        <!-- 右边内容部门 -->
        <div class="right col-lg-10 col-md-10 col-xs-10">
	        <div class="looktable">
	            <form id="userForm" action="" method="post" class="form-inline" style="margin-top: 60px;">
	                <table>
	                    <tr>
	                        <td class="zuotd " style="text-align: center;" colspan="2">信息修改</td>
	                    </tr>
	                    <tr>
	                        <td class="zuotd">编号:</td>
	                        <td class="youtd"><input type="text" name="id" value="${look.id }" disabled></td>
	                    </tr>
	                    <tr>
	                        <td class="zuotd">用户名:</td>
	                        <td class="youtd"><input type="text" name="username" value="${look.username }" disabled></td>
	                    </tr>
	                    <%-- <tr>
	                        <td class="zuotd">密码:</td>
	                        <td class="youtd"><input type="text" name="password" value="${look.password }"></td>
	                    </tr> --%>
	                    <tr>
	                        <td class="zuotd">身份证号码:</td>
	                        <td class="youtd"><input type="text" name="identity" value="${look.identity }"></td>
	                    </tr>
	                    <tr>
	                        <td class="zuotd">电话:</td>
	                        <td class="youtd"><input type="text" name="concat" value="${look.concat }"></td>
	                    </tr>
	                    <tr>
	                        <td class="zuotd">驾驶证号:</td>
	                        <td class="youtd"><input type="text" name="license" value="${look.license }"></td>
	                    </tr>
	                    <tr>
	                        <td class="zuotd">邮箱:</td>
	                        <td class="youtd"><input type="text" name="uemail" value="${look.uemail }"></td>
	                    </tr>
	                    <tr>
	                        <td class="zuotd">角色:</td>
	                        <td class="youtd"><input type="text" name="role" value="${look.role }"></td>
	                    </tr>
	                    <tr>
	                        <td colspan="2" class="text-center">
								<br>
								<a href="#" class="btn btn-primary update">保 存</a>
								<a href="<%=basePath%>user/list" class="btn btn-default">返 回</a>
	                        </td>
	                    </tr>
	                </table>
	            </form>
	        </div>
    	</div>
	</div>
</div>
<script src="http://www.jq22.com/jquery/1.11.1/jquery.min.js"></script>
<script src="http://www.jq22.com/jquery/bootstrap-3.3.4.js"></script>
<script src="../js/distpicker.data.js"></script>
<script src="../js/distpicker.js"></script>
<script src="../js/main.js"></script>
<script src="../js/userlook.js"></script>
<script type="text/javascript">
    $(function () {
        UserLook.init('<%=basePath%>');
    });
</script>
</body>
</html>