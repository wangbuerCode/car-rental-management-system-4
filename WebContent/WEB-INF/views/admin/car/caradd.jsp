<%@ page language="java" contentType="text/html; charset=utf-8"
         pageEncoding="utf-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
            + path + "/";
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <link href="http://www.jq22.com/jquery/bootstrap-3.3.4.css"
          rel="stylesheet">
    <!--[if IE]>
    <script src="http://libs.baidu.com/html5shiv/3.7/html5shiv.min.js"></script>
    <![endif]-->
	<link rel="stylesheet" href="https://cdn.staticfile.org/twitter-bootstrap/3.3.7/css/bootstrap.min.css">
	<link rel="stylesheet" href="<%=basePath %>assets/layui/css/layui.css">
	<link rel="stylesheet" href="<%=basePath %>/css/main.css">
    <title>汽车管理</title>
    <style type="text/css">
        .header a {
            color: white;
        }
    </style>
</head>
<body>
<div class="header" style="height: 55px;">
    <nav class="navbar navbar-inverse" role="navigation">
        <div class="navbar-inner" style="">
            <div class="navbar-header">
                <a class="navbar-brand" href="#" style="margin-right: 50px; margin-left: 20px;">车辆租赁系统</a>
            </div>
        <div>
        <p class="navbar-text navbar-left">
			当前时间： <span id="Date"></span>
        </p>
        <!--向右对齐-->
        <p class="navbar-text navbar-right" style="margin-right: 100px;">
        	<a href="<%=basePath %>front/logout">退出</a>
        </p>
        <ul class="nav navbar-nav navbar-right">
        	<li class="dropdown">
        		<a href="#" class="dropdown-toggle" data-toggle="dropdown"> admin <b class="caret"></b></a>
				<ul class="dropdown-menu">
                	<li><a href="#">jmeter</a></li>
                </ul>
			</li>
        </ul>
    </nav>
</div>

<div class="container-fluid">
    <div class="row">
        <!-- 左边查询框 -->
        <div class="left col-lg-2 col-md-2 col-xs-2">
            <ul class="nav nav-pills nav-stacked">
                <li><a href="<%=basePath%>admin/index">首页</a></li>
                <li class="active"><a href="<%=basePath%>car/list">汽车管理</a></li>
				<li><a href="<%=basePath%>order/list">订单管理</a></li>
                <li><a href="<%=basePath%>user/list">用户管理</a></li>
                <li><a href="#">维修管理</a></li>
				<li><a href="<%=basePath%>notice/list">公告管理</a></li>
<%-- 				<li><a href="<%=basePath%>user/modifypwd">修改密码</a></li> --%>
            </ul>
        </div>
        <!-- 右边内容部门 -->
        <div class="right col-lg-10 col-md-10 col-xs-10" >
        	<div class="looktable">
            	<form  class="form-inline" id="conditionForm" style="margin-top: 60px;">
	                <table>
	                    <tr>
	                        <td class="zuotd" colspan="2" style="text-align: center;">添加信息</td>
	                    </tr>
	                    <tr>
	                        <td class="zuotd">类型:</td>
	                        <td class="youtd" >
	                        	<select name="type" class="select-100">
	                        		<option value="SUV">SUV</option>
				                    <option value="商务型">商务型</option>
				                    <option value="电动型">电动型</option>
				                    <option value="精英型">精英型</option>
				                    <option value="经济型">经济型</option>
				                    <option value="舒适型">舒适型</option>
				                    <option value="高端车">高端车</option>
	                        	</select>
	                        </td>
	                    </tr>
	                    <tr>
	                        <td class="zuotd">品牌:</td>
	                        <td class="youtd">
	                        	<select name="brand" class="select-100">
	                        		<option value="MG">MG</option>
				                    <option value="一汽">一汽</option>
				                    <option value="一汽马自达">一汽马自达</option>
				                    <option value="东风">东风</option>
				                    <option value="丰田">丰田</option>
									<option value="凯迪拉克">凯迪拉克</option>
									<option value="别克">别克</option>
									<option value="大众">大众</option>
									<option value="大众斯柯达">大众斯柯达</option>
									<option value="奔驰">奔驰</option>
									<option value="奥迪">奥迪</option>
									<option value="宝马">宝马</option>
									<option value="捷豹">捷豹</option>
									<option value="本田">本田</option>
									<option value="标致">标致</option>
									<option value="玛莎拉蒂">玛莎拉蒂</option>
									<option value="现代">现代</option>
									<option value="福特">福特</option>
									<option value="纳智捷">纳智捷</option>
									<option value="绅宝">绅宝</option>
									<option value="腾势">腾势</option>
									<option value="英菲尼迪">英菲尼迪</option>
									<option value="荣威">荣威</option>
									<option value="起亚">起亚</option>
									<option value="路虎">路虎</option>
									<option value="雪佛兰">雪佛兰</option>
									<option value="马自达">马自达</option>
	                        	</select>
	                        </td>
	                    </tr>
	                    <tr>
	                        <td class="zuotd">租价:</td>
	                        <td class="youtd"><input type="text" name="renprice" placeholder="租价"></td>
	                    </tr>
	                    <tr>
	                        <td class="zuotd">押金:</td>
	                        <td class="youtd"><input type="text" name="deposit"  placeholder="押金"></td>
	                    </tr>
	                    <tr>
	                        <td class="zuotd">违章押金:</td>
	                        <td class="youtd"><input type="text" name="peccancy"  placeholder="违章押金"></td>
	                    </tr>
	                    <tr>
	                        <td class="zuotd">所属地区:</td>
	                        <td class="youtd">
	                            <div data-toggle="distpicker">
	                                <div class="form-group" style="padding-top: 2px; width: 174px;">
	                                    <label class="sr-only" for="province">Province</label>
	                                    <select name="province" class="select-100" id="province1" data-province=""></select>
	                                </div>
	                                </br>
	                                <div class="form-group" style="padding-top: 2px; width: 174px;">
	                                    <label class="sr-only" for="city">City</label>
	                                    <select name="city" class="select-100" id="city1" data-city="">></select>
	                                </div>
	                                </br>
	                                <div class="form-group" style="padding-top: 2px; width: 174px;">
	                                    <label class="sr-only" for="area">District</label>
	                                    <select name="area" class="select-100" id="district1" data-district=""></select>
	                                </div>
	                            </div>
	                        </td>
	                    </tr>
	                    <tr>
	                        <td class="zuotd">所属门店:</td>
	                        <td class="youtd">
	                            <select name="jtstore" class="select-100" id="shop">
								</select>
	                        </td>
	                    </tr>
	                    <tr>
	                        <td class="zuotd">租车状态:</td>
	                        <td class="youtd">
	                        	<select name="renstatus" class="select-100">
	                        		<option value="已租出">已租出</option>
	                        		<option value="未租出">未租出</option>
	                        	</select>
	                        </td>
	                    </tr>
	                    <tr>
	                        <td class="zuotd">车辆状态:</td>
	                        <td class="youtd">
	                        	<select name="carstatus" class="select-100">
	                        	<option value="">请选择</option>
	                        		<option value="正常">正常</option>
	                        		<option value="未租出">维修中</option>
	                        	</select>
	                        </td>
	                    </tr>
	                    <tr>
	                        <td colspan="2" class="text-center">
								<br>
								<a href="#" class="btn btn-primary addCar" role="button">添 加</a>
								<a href="<%=basePath%>car/list" class="btn btn-default" role="button">返 回</a>
	                        </td>
	                    </tr>
	                </table>
            	</form>
        	</div>
    	</div>
    </div>
</div>
<script src="https://cdn.staticfile.org/jquery/2.1.1/jquery.min.js"></script>
<script src="https://cdn.staticfile.org/twitter-bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script src="<%=basePath %>/js/distpicker.data.js"></script>
<script src="<%=basePath %>/js/distpicker.js"></script>
<script src="<%=basePath %>/js/main.js"></script>
<script src="<%=basePath %>/js/caradd.js"></script>
<script type="text/javascript">
    $(function () {
        CarAdd.init('<%=basePath%>');
    });
</script>
</body>
</html>