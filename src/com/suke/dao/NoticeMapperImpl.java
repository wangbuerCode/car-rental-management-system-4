package com.suke.dao;

import com.suke.dto.DataGridDto;
import com.suke.pojo.Notice;
import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class NoticeMapperImpl implements NoticeMapper {
	@Autowired
	private SqlSessionTemplate sqlSession;

	@Override
	public List<Notice> selectByCondition(DataGridDto dataGridDto) {
		return sqlSession.getMapper(NoticeMapper.class).selectByCondition(dataGridDto);
	}

	@Override
	public Integer countByCondition(DataGridDto dataGridDto) {
		return sqlSession.getMapper(NoticeMapper.class).countByCondition(dataGridDto);
	}


	@Override
	public Notice select(Integer id) {
		return sqlSession.getMapper(NoticeMapper.class).select(id);
	}

	@Override
	public void delete(Integer id) {
		sqlSession.getMapper(NoticeMapper.class).delete(id);
	}

	@Override
	public void insert(Notice notice) {
		sqlSession.getMapper(NoticeMapper.class).insert(notice);
	}

	@Override
	public void update(Notice notice) {
		sqlSession.getMapper(NoticeMapper.class).update(notice);
	}
}
