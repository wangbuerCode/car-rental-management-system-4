package com.suke.dao;

import com.suke.dto.DataGridDto;
import com.suke.dto.PageDto;
import com.suke.pojo.User;
import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class UserMapperImpl implements UserMapper{
	@Autowired
	private SqlSessionTemplate sqlSession;

	@Override
	public List<User> selectByCondition(DataGridDto dataGridDto) {
		
		return sqlSession.getMapper(UserMapper.class).selectByCondition(dataGridDto);
	}

	@Override
	public Integer countByCondition(DataGridDto dataGridDto) {
		return sqlSession.getMapper(UserMapper.class).countByCondition(dataGridDto);
	}

	@Override
	public void deleteUser(Integer id) {
		sqlSession.getMapper(UserMapper.class).deleteUser(id);
	}

	@Override
	public void insertUser(User user) {
		sqlSession.getMapper(UserMapper.class).insertUser(user);
	}

	@Override
	public User lookUser(Integer id) {
		return sqlSession.getMapper(UserMapper.class).lookUser(id);
	}

	@Override
	public User lookUserByUname(String userName) {
		return sqlSession.getMapper(UserMapper.class).lookUserByUname(userName);
	}

	@Override
	public void updateUser(User user) {
		sqlSession.getMapper(UserMapper.class).updateUser(user);
	}

	@Override
	public void modifypwd(User user) {
		sqlSession.getMapper(UserMapper.class).modifypwd(user);
	}
}
