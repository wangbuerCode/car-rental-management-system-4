package com.suke.dao;

import com.suke.dto.DataGridDto;
import com.suke.pojo.User;

import java.util.List;

public interface UserMapper {
	//分页查询user总记录数
	/**
	 * 条件查询
	 * @author dyb
	 * @param dataGridDto 分页条件
	 * @return list
	 */
	List<User> selectByCondition(DataGridDto dataGridDto);

	Integer countByCondition(DataGridDto dataGridDto);
	
	void deleteUser(Integer id);

	void updateUser(User user);

	void insertUser(User user);

	User lookUser(Integer id);

	User lookUserByUname(String userName);
	
	//修改密码
	void modifypwd(User user);
	
	

}
