package com.suke.dao;

import java.util.List;

import com.suke.dto.DataGridDto;
import com.suke.dto.PageDto;
import com.suke.pojo.Car;

public interface CarMapper {
	

	/**
	 * 条件查询
	 * @author dyb
	 * @param dataGridDto 分页条件
	 * @return list
	 */
	List<Car> selectByCondition(DataGridDto dataGridDto);

	Integer countByCondition(DataGridDto dataGridDto);
	
	//编辑
	Car lookCar(Integer id);
	//删除
	void deleteCar(Integer id);
	//添加
	void insertCar(Car car) ;
	//更新
	void updateCar(Car car) ;

}
