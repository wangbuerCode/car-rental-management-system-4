package com.suke.dao;

import com.suke.dto.DataGridDto;
import com.suke.pojo.Car;
import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class CarMapperImpl implements CarMapper {
	@Autowired
	private SqlSessionTemplate sqlSession;
    
	@Override
	public Car lookCar(Integer id) {
		return sqlSession.getMapper(CarMapper.class).lookCar(id);
	}

	@Override
	public void deleteCar(Integer id) {
		sqlSession.getMapper(CarMapper.class).deleteCar(id);
	}

	@Override
	public List<Car> selectByCondition(DataGridDto dataGridDto) {
		return sqlSession.getMapper(CarMapper.class).selectByCondition(dataGridDto);
	}

	@Override
	public Integer countByCondition(DataGridDto dataGridDto) {
		return sqlSession.getMapper(CarMapper.class).countByCondition(dataGridDto);
	}

	@Override
	public void insertCar(Car car) {
		 sqlSession.getMapper(CarMapper.class).insertCar(car);
	}

	@Override
	public void updateCar(Car car) {
		sqlSession.getMapper(CarMapper.class).updateCar(car);
	}


}
