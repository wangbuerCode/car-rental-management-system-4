package com.suke.dao;

import com.suke.dto.DataGridDto;
import com.suke.pojo.Renorder;
import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class RenorderMapperImpl implements RenorderMapper{
	@Autowired
	private SqlSessionTemplate sqlSession;

	@Override
	public List<Renorder> selectByCondition(DataGridDto dataGridDto) {
		return sqlSession.getMapper(RenorderMapper.class).selectByCondition(dataGridDto);
	}

	@Override
	public Integer countByCondition(DataGridDto dataGridDto) {
		return sqlSession.getMapper(RenorderMapper.class).countByCondition(dataGridDto);
	}

	@Override
	public void delete(Integer id) {
		sqlSession.getMapper(RenorderMapper.class).delete(id);
	}

	@Override
	public void insert(Renorder renorder) {
		sqlSession.getMapper(RenorderMapper.class).insert(renorder);
	}

	@Override
	public Renorder select(Integer id) {
		return sqlSession.getMapper(RenorderMapper.class).select(id);
	}

	@Override
	public void update(Renorder renorder) {
		sqlSession.getMapper(RenorderMapper.class).update(renorder);
	}
}
