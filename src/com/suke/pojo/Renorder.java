package com.suke.pojo;

import java.util.Date;

public class Renorder {
	private Integer id;
	private Integer userid;
	private String userName;
	private Integer carid;
	private String brand;
	private Double deposit;//押金
	private Double renprice;//租车价格
	private Float renday;//租车天数
	private Double totalprice;//订单总额

	private Date pickdate;//取车时间
	//取车门店
	private String province;//省
	private String city;//市
	private String area;//区
	private String jtstore;//具体门店
	private Date stilldate;//还车时间
	private Date realgive;//实际还车时间
	private Double fine;//罚款
	private String overdue;//是否逾期归还
	private Integer givestatus;//还车状态 0是未还车 1是还车
	private String paymethod;//支付方式
	private Date createtime;//创建时间
	private String auditing;//审核状态
	private String beizhu;//备注
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Integer getUserid() {
		return userid;
	}
	public void setUserid(Integer userid) {
		this.userid = userid;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public Integer getCarid() {
		return carid;
	}
	public void setCarid(Integer carid) {
		this.carid = carid;
	}
	public String getBrand() {
		return brand;
	}
	public void setBrand(String brand) {
		this.brand = brand;
	}
	public Double getDeposit() {
		return deposit;
	}
	public void setDeposit(Double deposit) {
		this.deposit = deposit;
	}
	public Double getRenprice() {
		return renprice;
	}
	public void setRenprice(Double renprice) {
		this.renprice = renprice;
	}
	public Float getRenday() {
		return renday;
	}
	public void setRenday(Float renday) {
		this.renday = renday;
	}
	public Double getTotalprice() {
		return totalprice;
	}
	public void setTotalprice(Double totalprice) {
		this.totalprice = totalprice;
	}
	public Date getPickdate() {
		return pickdate;
	}
	public void setPickdate(Date pickdate) {
		this.pickdate = pickdate;
	}
	public String getProvince() {
		return province;
	}
	public void setProvince(String province) {
		this.province = province;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getArea() {
		return area;
	}
	public void setArea(String area) {
		this.area = area;
	}
	public String getJtstore() {
		return jtstore;
	}
	public void setJtstore(String jtstore) {
		this.jtstore = jtstore;
	}
	public Date getStilldate() {
		return stilldate;
	}
	public void setStilldate(Date stilldate) {
		this.stilldate = stilldate;
	}
	public Date getRealgive() {
		return realgive;
	}
	public void setRealgive(Date realgive) {
		this.realgive = realgive;
	}
	public Double getFine() {
		return fine;
	}
	public void setFine(Double fine) {
		this.fine = fine;
	}
	public String getOverdue() {
		return overdue;
	}
	public void setOverdue(String overdue) {
		this.overdue = overdue;
	}
	public Integer getGivestatus() {
		return givestatus;
	}
	public void setGivestatus(Integer givestatus) {
		this.givestatus = givestatus;
	}
	public String getPaymethod() {
		return paymethod;
	}
	public void setPaymethod(String paymethod) {
		this.paymethod = paymethod;
	}
	public Date getCreatetime() {
		return createtime;
	}
	public void setCreatetime(Date createtime) {
		this.createtime = createtime;
	}
	public String getAuditing() {
		return auditing;
	}
	public void setAuditing(String auditing) {
		this.auditing = auditing;
	}
	public String getBeizhu() {
		return beizhu;
	}
	public void setBeizhu(String beizhu) {
		this.beizhu = beizhu;
	}

}
