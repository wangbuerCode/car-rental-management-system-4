package com.suke.pojo;

public class Car {
	private Integer id;
	private String type;//类型
	private String brand;//品牌
	private Double renprice;//租车价格
	private Double deposit;//押金
	private Double peccancy;//违章押金
	private String province;//省
	private String city;//市
	private String area;//区
	private String jtstore;//具体门店
	private String renstatus;//租车状态
	private String carstatus;//车辆状态
	public String tupian;//图片存储
	
	public Car() {
		super();
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getBrand() {
		return brand;
	}
	public void setBrand(String brand) {
		this.brand = brand;
	}
	public Double getRenprice() {
		return renprice;
	}
	public void setRenprice(Double renprice) {
		this.renprice = renprice;
	}
	public Double getDeposit() {
		return deposit;
	}
	public void setDeposit(Double deposit) {
		this.deposit = deposit;
	}
	public Double getPeccancy() {
		return peccancy;
	}
	public void setPeccancy(Double peccancy) {
		this.peccancy = peccancy;
	}
	public String getProvince() {
		return province;
	}
	public void setProvince(String province) {
		this.province = province;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getArea() {
		return area;
	}
	public void setArea(String area) {
		this.area = area;
	}
	public String getJtstore() {
		return jtstore;
	}
	public void setJtstore(String jtstore) {
		this.jtstore = jtstore;
	}
	
	public String getRenstatus() {
		return renstatus;
	}
	public void setRenstatus(String renstatus) {
		this.renstatus = renstatus;
	}
	public String getCarstatus() {
		return carstatus;
	}
	public void setCarstatus(String carstatus) {
		this.carstatus = carstatus;
	}
	public String getTupian() {
		return tupian;
	}
	public void setTupian(String tupian) {
		this.tupian = tupian;
	}
	
	
}
