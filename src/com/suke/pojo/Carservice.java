package com.suke.pojo;

import java.util.Date;

public class Carservice {
	private Integer id;
	private Integer cid;//车的外键
	private Double spendmoney;//修理花费
	private Date servicedate;
	private String servicemark;//备注
	
	public Carservice() {
		super();
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	
	public Integer getCid() {
		return cid;
	}
	public void setCid(Integer cid) {
		this.cid = cid;
	}
	public Double getSpendmoney() {
		return spendmoney;
	}
	public void setSpendmoney(Double spendmoney) {
		this.spendmoney = spendmoney;
	}
	public Date getServicedate() {
		return servicedate;
	}
	public void setServicedate(Date servicedate) {
		this.servicedate = servicedate;
	}
	public String getServicemark() {
		return servicemark;
	}
	public void setServicemark(String servicemark) {
		this.servicemark = servicemark;
	}
	

}
