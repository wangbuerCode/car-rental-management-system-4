package com.suke.controller;

import com.suke.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * 后台首页
 */
@Controller
@RequestMapping("/admin")
public class AdminController {
	@Autowired
	private UserService userService;

	@RequestMapping("/index")
	public String index() {
		return "admin/index";
	}

}
