package com.suke.controller;


import com.suke.dto.DataGridDto;
import com.suke.dto.PageDto;
import com.suke.dto.ResultDto;
import com.suke.pojo.Renorder;
import com.suke.pojo.User;
import com.suke.service.RenorderService;
import com.suke.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;

/**
 * 订单管理
 *
 * @author wuxueli
 */
@Controller
@RequestMapping("/order")
public class RenorderController {
    @Autowired
    private UserService userService;
    @Autowired
    private RenorderService renorderService;

    /**
     * 管理界面
     * @author wuxueli
     * @return
     */
    @RequestMapping("/list")
    public String carList() {
        return "admin/order/list";
    }

    /**
     * 查询所有
     * @param dataGridDto
     * @author wuxueli
     */
    @RequestMapping(value = "/page", method = RequestMethod.POST)
    @ResponseBody
    public Object page(@RequestBody DataGridDto dataGridDto) {
        ResultDto resultDto = new ResultDto();
        PageDto page = renorderService.selectByCondition(dataGridDto);
        resultDto.setData(page);
        resultDto.setMessage("查询成功");
        resultDto.setCode(200);
        return resultDto;
    }
    
    /**
     * 订单添加
     * @param renorder
     * @author wuxueli
     */
    @RequestMapping(value = "/insert", method = RequestMethod.POST)
    @ResponseBody
    public Object add(HttpServletRequest request, @RequestBody Renorder renorder) {
        String userName = (String) request.getSession().getAttribute(request.getSession().getId());
        ResultDto resultDto = new ResultDto();
        if (StringUtils.isEmpty(userName)) {
        	resultDto.resultError();
            resultDto.setMessage("用户未登录");
            resultDto.setCode(200);
            return resultDto;
        }
        User user = userService.lookUserByUname(userName);
        if (user == null) {
            resultDto.setMessage("用户不存在");
            resultDto.setCode(200);
            return resultDto;
        }
//        renorder.setPickdate(new Date(renorder.));
        renorder.setUserid(user.getId());
        renorder.setAuditing("待审核");
        renorder.setCreatetime(new Date());
        renorderService.insert(renorder);
        resultDto.setMessage("添加成功");
        resultDto.setCode(200);
        return resultDto;
    }

    /**
     * 编辑
     *
     * @param id
     * @author wuxueli
     */
    @RequestMapping(value = "/look")
    public ModelAndView lookCar(Integer id) {
        ModelAndView view = new ModelAndView();
        Renorder renorder = renorderService.select(id);
        view.addObject("look", renorder);
        view.setViewName("admin/order/look");
        return view;
    }

    /**
     * 删除
     *
     * @param id
     * @author wuxueli
     */
    @RequestMapping(value = "/delete", method = RequestMethod.GET)
    @ResponseBody
    public ResultDto deleteCar(Integer id) {
        renorderService.delete(id);
        ResultDto resultDto = new ResultDto();
        resultDto.setMessage("删除成功");
        resultDto.setCode(200);
        return resultDto;
    }

    /**
     * 更新
     * @param renorder
     * @return
     */
    @RequestMapping(value="/update", method = RequestMethod.POST)
    @ResponseBody
    public ResultDto updateCar(@RequestBody Renorder renorder) {
        renorderService.update(renorder);
        ResultDto resultDto = new ResultDto();
        resultDto.setMessage("保存成功");
        resultDto.setCode(200);
        return resultDto;
    }

    /**
     * 审核
     * @param orderId
     * @return
     */
    @RequestMapping(value="/check", method = RequestMethod.GET)
    @ResponseBody
    public ResultDto updateCar(@RequestParam int orderId) {
        Renorder renorder = renorderService.select(orderId);
        ResultDto resultDto = new ResultDto();
        if (renorder == null) {
            resultDto.setMessage("保存成功");
            resultDto.setCode(200);
            return resultDto;
        }
        renorder.setAuditing("审核通过");
        renorderService.update(renorder);
        resultDto.setMessage("审核成功");
        resultDto.setCode(200);
        return resultDto;
    }

    /**
     * 我的订单 列表的显示，罚金的计算
     * @param dataGridDto
     * @author wuxueli
     */
    @RequestMapping(value = "/myOrderPage", method = RequestMethod.POST)
    @ResponseBody
    public Object myOrderPage(HttpServletRequest request, @RequestBody DataGridDto dataGridDto) {
        ResultDto resultDto = new ResultDto();
        String userName = (String) request.getSession().getAttribute(request.getSession().getId());
        if (StringUtils.isEmpty(userName)) {
        	resultDto.resultError();
        	 resultDto.setMessage("用户未登录");
             resultDto.setCode(200);
             return resultDto;
        }
        User user = userService.lookUserByUname(userName);
        if (user == null) {
        	resultDto.resultError();
            resultDto.setMessage("用户不存在");
            resultDto.setCode(200);
            return resultDto;
        }
        dataGridDto.getParams().put("userid", user.getId());
        
        //在reorderservice  计算罚金
        PageDto page = renorderService.selectByCondition(dataGridDto);
        resultDto.setData(page);
        resultDto.setMessage("查询成功");
        resultDto.setCode(200);
        return resultDto;
    }
}
