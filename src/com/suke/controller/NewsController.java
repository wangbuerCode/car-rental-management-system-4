package com.suke.controller;


import com.suke.dto.DataGridDto;
import com.suke.dto.PageDto;
import com.suke.dto.ResultDto;
import com.suke.pojo.Notice;
import com.suke.service.NoticeService;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

/**
 * 公告管理
 *
 * @author wuxueli
 */
@Controller
@RequestMapping("/notice")
public class NewsController {
    @Autowired
    private NoticeService noticeService;

    /**
     * 公告管理界面
     * @author wuxueli
     * @return
     */
    @RequestMapping("/list")
    public String carList() {
        return "admin/notice/list";
    }

    /**
     * 查询所有
     * @param dataGridDto
     * @author wuxueli
     */
    @RequestMapping(value = "/page", method = RequestMethod.POST)
    @ResponseBody
    public Object page(@RequestBody DataGridDto dataGridDto) {
        ResultDto resultDto = new ResultDto();
        PageDto page = noticeService.selectByCondition(dataGridDto);
        resultDto.setData(page);
        resultDto.setMessage("查询成功");
        resultDto.setCode(200);
        return resultDto;
    }

    /**
     * 编辑
     *
     * @param id
     * @author wuxueli
     */
    @RequestMapping(value = "/look")
    public ModelAndView lookCar(Integer id) {
        ModelAndView view = new ModelAndView();
        Notice notice = noticeService.select(id);
        view.addObject("look", notice);
        view.setViewName("admin/notice/look");
        return view;
    }

    /**
     * 删除
     *
     * @param id
     * @author wuxueli
     */
    @RequestMapping(value = "/delete", method = RequestMethod.GET)
    @ResponseBody
    public ResultDto deleteCar(Integer id) {
        noticeService.delete(id);
        ResultDto resultDto = new ResultDto();
        resultDto.setMessage("删除成功");
        resultDto.setCode(200);
        return resultDto;
    }
    /**
     * 添加页面
     * @return
     */
    @RequestMapping("/toAdd")
    public String carAdd() {
        return "admin/notice/add";
    }

    /**
     * 添加
     * @param notice
     * @return
     */
    @RequestMapping(value="/insert", method = RequestMethod.POST)
    @ResponseBody
    public ResultDto insertCar(@RequestBody Notice notice) {
    	notice.setReleasedate(new Date());
        noticeService.insert(notice);
        ResultDto resultDto = new ResultDto();
        resultDto.setMessage("添加成功");
        resultDto.setCode(200);
        return resultDto;
    }

    /**
     * 更新
     * @param notice
     * @return
     */
    @RequestMapping(value="/update", method = RequestMethod.POST)
    @ResponseBody
    public ResultDto updateCar(@RequestBody Notice notice) {
        noticeService.update(notice);
        ResultDto resultDto = new ResultDto();
        resultDto.setMessage("保存成功");
        resultDto.setCode(200);
        return resultDto;
    }
}
