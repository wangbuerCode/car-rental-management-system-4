package com.suke.service;

import com.suke.dao.CarMapper;
import com.suke.dto.DataGridDto;
import com.suke.dto.PageDto;
import com.suke.pojo.Car;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CarService {
    @Autowired
    private CarMapper carMapper;

    /**
     * 条件查询
     *
     * @param
     * @return
     * @author dyb
     */
    public PageDto<Car> selectByCondition(DataGridDto dataGridDto) {
        PageDto<Car> page = new PageDto<>();
        List<Car> list = carMapper.selectByCondition(dataGridDto);
        int count = carMapper.countByCondition(dataGridDto);
        page.setPageSize(dataGridDto.getLineSize());
        page.setTotalDataCount(count);
        page.setData(list);
        return page;
    }

    /**
     * 编辑第一步
     *
     * @param id
     * @author dyb
     */
    public Car lookCar(Integer id) {
        return carMapper.lookCar(id);
    }

    /**
     * 删除
     * @author dyb
     * @param id
     */
    public void deleteCar(Integer id) {
        carMapper.deleteCar(id);
    }
    /**
     * 添加
     * @param car
     */
    public void insertCar(Car car) {
    	carMapper.insertCar(car);
    }

    /**
     * 更新
     * @param car
     */
    public void updateCar(Car car) {
    	carMapper.updateCar(car);
    }


}
