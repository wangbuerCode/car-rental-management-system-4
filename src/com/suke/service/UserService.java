package com.suke.service;

import com.suke.dao.UserMapper;
import com.suke.dto.DataGridDto;
import com.suke.dto.PageDto;
import com.suke.pojo.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class UserService {
	@Autowired
	private UserMapper userMapper;
	//分页查询所有car记录数
	  /**
     * 条件查询
     *
     * @param
     * @return
     * @author dyb
     */
    public PageDto<User> selectByCondition(DataGridDto dataGridDto) {
        PageDto<User> page = new PageDto<>();
        List<User> list = userMapper.selectByCondition(dataGridDto);
        int count = userMapper.countByCondition(dataGridDto);
        page.setPageSize(dataGridDto.getLineSize());
        page.setTotalDataCount(count);
        page.setData(list);
        return page;
    }
    /**
     * 删除
     * @author dyb
     * @param id
     */
    public void deleteUser(Integer id) {
        userMapper.deleteUser(id);
    }

    /**
     * 添加
     * @param user
     * @author dyb
     */
    public void insertUser(User user) {
        userMapper.insertUser(user);
    }

    /**
     * 编辑第一步
     *
     * @param id
     * @author dyb
     */
    public User lookUser(Integer id) {
        return userMapper.lookUser(id);
    }

    public User lookUserByUname(String userName) {
        return userMapper.lookUserByUname(userName);
    }

    public void updateUser(User user) {
        userMapper.updateUser(user);
    }
    public void modifypwd(User user) {
        userMapper.modifypwd(user);
    }

}
